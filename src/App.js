import React, { useState, useEffect } from "react";
import bridge from "@vkontakte/vk-bridge";
import {
  View,
  ScreenSpinner,
  AdaptivityProvider,
  AppRoot,
  Panel,
  PanelHeader,
  Header,
  Button,
  Group,
  FixedLayout,
} from "@vkontakte/vkui";
import "@vkontakte/vkui/dist/vkui.css";

import Home from "./Home";
import generate_maze from "./generate_maze";

const App = () => {
  const [maze, setMaze] = useState(null);
  const [fetchedUser, setUser] = useState(null);
  const [popout, setPopout] = useState(<ScreenSpinner size="large" />);

  const cw = document.body.clientWidth,
    ch = document.body.clientHeight * 0.7;

  useEffect(() => {
    bridge.subscribe(({ detail: { type, data } }) => {
      if (type === "VKWebAppUpdateConfig") {
        const schemeAttribute = document.createAttribute("scheme");
        schemeAttribute.value = data.scheme ? data.scheme : "client_light";
        document.body.attributes.setNamedItem(schemeAttribute);
      }
    });
    async function fetchData() {
      const user = await bridge.send("VKWebAppGetUserInfo");
      setUser(user);
      setPopout(null);
    }
    fetchData();
  }, []);

  return (
    <AdaptivityProvider>
      <AppRoot>
        <View activePanel="home" popout={popout}>
          <Panel id="home">
            <PanelHeader>Лабиринт</PanelHeader>
            {fetchedUser && (
              <Group
                header={
                  <Header mode="secondary">
                    {fetchedUser.first_name}, используйте гироскоп, чтобы играть
                  </Header>
                }
              >
                {maze && <Home labyrinth={maze} cw={cw} ch={ch} />}
              </Group>
            )}
            <FixedLayout filled vertical="bottom">
              <Button
                stretched
                mode="primary"
                size="m"
                onClick={() => setMaze(generate_maze(cw, ch))}
              >
                {maze === null ? "Начать" : "Заново"}
              </Button>
            </FixedLayout>
          </Panel>
        </View>
      </AppRoot>
    </AdaptivityProvider>
  );
};

export default App;
