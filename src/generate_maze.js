import generator from "generate-maze";
import { Engine, Render, Bodies, World } from "matter-js";

const wallSize = 5;

export default (width, height, cnt = 8) => {
  width = height = Math.min(width, height);
  let cellSize = Math.round(width / cnt);

  let res = generator(cnt, cnt, true, Math.random() * 10000).flatMap((i) =>
    i.flatMap(({ x, y, top, left }) => {
      var res = [];

      top &&
        res.push(
          Bodies.rectangle(
            (x * width) / cnt + cellSize / 2,
            (y * height) / cnt + wallSize / 2,
            cellSize,
            wallSize,
            {
              isStatic: true,
              render: {
                fillStyle: "#3300ff",
              },
            }
          )
        );
      left &&
        res.push(
          Bodies.rectangle(
            (x * width) / cnt + wallSize / 2,
            (y * height) / cnt + cellSize / 2 + wallSize / 2,
            wallSize,
            cellSize + wallSize,
            {
              isStatic: true,
              render: {
                fillStyle: "#3300ff",
              },
            }
          )
        );
      return res;
    })
  );

  res.push(
    Bodies.rectangle(width - wallSize / 2, height / 2, wallSize, height, {
      isStatic: true,
      render: {
        fillStyle: "#3300ff",
      },
    }),
    Bodies.rectangle(width / 2, height + wallSize / 2, width, wallSize, {
      isStatic: true,
      render: {
        fillStyle: "#3300ff",
      },
    })
  );

  const genCenterCellPos = (dim) =>
    Math.round(Math.random() * (cnt - 1)) * cellSize + cellSize / 2;

  return {
    maze: res,
    begin: [cellSize / 2, cellSize / 2],
    end: [width - cellSize / 2, height - cellSize / 2],
    // begin: [genCenterCellPos(width), genCenterCellPos(height)],
    // end: [genCenterCellPos(width), genCenterCellPos(height)],
  };
};
