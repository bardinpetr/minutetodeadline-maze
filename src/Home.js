import Matter, { Engine, Render, Bodies, World } from "matter-js";
import { React, useEffect, useRef, useState } from "react";
import bridge from "@vkontakte/vk-bridge";
import { Snackbar } from "@vkontakte/vkui";

const Home = ({ labyrinth, cw, ch }) => {
  const scene = useRef();
  const engine = useRef(Engine.create());
  const [snackbar, setSnackbar] = useState(null);

  useEffect(() => {
    const render = Render.create({
      element: scene.current,
      engine: engine.current,
      options: {
        width: cw,
        height: ch,
        wireframes: false,
        background: "transparent",
      },
    });

    Matter.Runner.run(engine.current);
    Render.run(render);

    const finish = () => {
      bridge.send("VKWebAppDeviceMotionStop", {});
      Render.stop(render);
      World.clear(engine.current.world);
      Engine.clear(engine.current);
      render.canvas.remove();
      render.canvas = null;
      render.context = null;
      render.textures = {};
    };

    bridge.send("VKWebAppDeviceMotionStart", { refresh_rate: 200 });

    bridge.subscribe(({ detail: { type, data } }) => {
      if (type === "VKWebAppDeviceMotionChanged") {
        engine.current.gravity.x = data.gamma;
        engine.current.gravity.y = -data.beta;
      }
    });

    Matter.Events.on(engine.current, "collisionStart", (event) => {
      event.pairs.forEach((i) => {
        if (
          i.bodyA.playerMode &&
          i.bodyB.playerMode &&
          i.bodyA.playerMode * i.bodyB.playerMode == 2
        ) {
          if (snackbar) return;
          World.clear(engine.current.world);
          setSnackbar(
            <Snackbar
              onClose={() => {
                setSnackbar(null);
              }}
              action="ОК"
            >
              Вы выиграли!
            </Snackbar>
          );
        }
      });
    });

    return finish;
  }, []);

  useEffect(() => {
    try {
      World.clear(engine.current.world);
      World.add(engine.current.world, [
        ...labyrinth.maze,
        Bodies.rectangle(...labyrinth.end, 20, 20, {
          isStatic: true,
          render: {
            fillStyle: "#00ff00",
          },
          playerMode: 1,
        }),
        Bodies.circle(...labyrinth.begin, 7, {
          mass: 10,
          restitution: 0.9,
          friction: 0.005,
          render: {
            fillStyle: "#ff0000",
          },
          playerMode: 2,
        }),
      ]);
    } catch {}
  }, [labyrinth]);

  return (
    <div>
      <div ref={scene} style={{ height: `${ch}px`, width: `${cw}px` }} />
      {snackbar}
    </div>
  );
};

export default Home;
